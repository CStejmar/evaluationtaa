%_____Objective evaluation of anti-aliasing methods_____
% Author: Carl Stejmar
 
close all;
clear all;
%%
map = [0, 0, 0;
       1, 0, 0];
MSE = zeros(1, 8);

imageRefStr = 'evaluation_images/scene02_08.png';
imRef=im2double(rgb2gray(imread(imageRefStr)));

figure(1);
imshow(imRef); title('8xMSAA'); colormap gray;

for n = 0:7
    
    nStr = int2str(n);
    imageStr = ['evaluation_images/scene02_0' nStr '.png'];
    
    im=im2double(rgb2gray(imread(imageStr)));
    
    titleStr = '';
    if n == 0
        titleStr = 'No AA';
    elseif n == 1
        titleStr = 'GBAA';
    elseif n == 2
        titleStr = 'FXAA';
    elseif n == 3
        titleStr = 'FXAACS';
    elseif n == 4
        titleStr = 'T2MSAA';
    elseif n == 5
        titleStr = 'ASAA';
    elseif n == 6
        titleStr = '2xMSAA';
    elseif n == 7
        titleStr = '4xMSAA';
    end
    
    figure((n + 1) + 1); % figure 2-9
    imshow(im); title(titleStr); colormap gray;
    
    imDiff = abs(imRef-im);
    
    figure((4*n + 9) + 1);
    imshow(imDiff); title('Image difference'); colormap gray;
    
    ImRefFourier = fftshift(fft2(imRef));
    ImFourier = fftshift(fft2(im));
    
    ImRefFourier = log(abs(ImRefFourier)+1); % Use log, for perceptual scaling, and +1 since log(0) is undefined
    ImRefFourier = mat2gray(ImRefFourier);
    
    ImFourier = log(abs(ImFourier)+1); % Use log, for perceptual scaling, and +1 since log(0) is undefined
    ImFourier = mat2gray(ImFourier);
    
    figure((4*n + 9) + 2);
    imshow(ImRefFourier); title('Fourier transform of reference image (8xMSAA)'); colormap gray;
    
    figure((4*n + 9) + 3);
    titleStrFourier = ['Fourier transform of ' titleStr];
    imshow(ImFourier); title(titleStrFourier); colormap gray;
    
    ImDiffFourier = abs(ImFourier-ImRefFourier);
    
    [sizeY, sizeX] = size(ImDiffFourier);
    
    for x = 1:sizeX
        for y = 1:sizeY
            if(ImDiffFourier(y,x) > 0.05)
                ImDiffFourier(y,x) = 1.0;
            end
        end
    end
    
    figure((4*n + 9) + 4);
    titleStrFourierDiff = ['Image difference between ' titleStr ' and ground truth in the Fourier domain'];
    imshow(ImDiffFourier); title(titleStrFourierDiff); %colormap hot;
    colormap(map); colorbar;
    
    MSE(1,n+1) = sum(mean((ImRefFourier - ImFourier).^2));
    
end

% Print result
MSE